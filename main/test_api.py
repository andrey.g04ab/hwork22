import pytest
from api.dbwork import db,User,Item
from api.routes import view,item
from flask import Flask
from api import Test,init_db

app = Flask(__name__)
app.config.from_object(Test)
init_db(app)

app.register_blueprint(view)
app.register_blueprint(item,url_prefix='/items')

@pytest.fixture
def client():
    yield app.test_client()
    with app.app_context():
        Item.query.delete()
        User.query.delete()
        db.session.commit()

def test_index(client):
    res=client.get('/')
    assert res.status_code==200
    assert b'working dbapi_22' in res.data

def test_reg(client):
    with app.app_context():
        data={'login':'a1','password':'a1'}
        res=client.post('/registration',json=data)
        assert User.query.count()==1
        assert res.json['status']==1

def test_add(client):
    with app.app_context():
        data={'login':'a1','password':'a1'}
        res=client.post('/registration',json=data)
        assert res.json['status']==1
        res=client.post('/login',json=data)
        print(res.json)
        data1={'tag':res.json['tag'],'name':'test'}
        res=client.post('/items/new',json=data1)
        assert Item.query.count()==1
        assert res.json['status']==1
        assert res.json['name']=='test'

if __name__=="__main__":
    pytest.main()        






