from api import db,User,Item
from itsdangerous import TimedJSONWebSignatureSerializer as Ttoken
import sentry_sdk
sentry_sdk.init(
    "https://21c4b0cdf7aa457da33f870945b3090b@o1092289.ingest.sentry.io/6137894",

    # Set traces_sample_rate to 1.0 to capture 100%
    # of transactions for performance monitoring.
    # We recommend adjusting this value in production.
    traces_sample_rate=1.0
)

class Dbwork():
    def __init__(self, db) -> None:
        self.dbase = db
        self.stoken=Ttoken('hw_22_key',1200)
            
    def getusr(self,tok:str):    
        try:
           id=self.stoken.loads(tok)['usr_id'] 
           return id
        except:
            return None   

    def gettok(self,log:str,passw:str):
        try:
            rec= User.query.filter_by(login=log.upper()).first()
            if rec and rec.passwrd==passw: 
                return self.stoken.dumps({'usr_id':rec.id}).decode('utf-8')
            return None   
        except Exception as exc:
            sentry_sdk.capture_exception(exc)
            return None   

    def adduser(self,log:str,passw:str):
        try:
            rec = User.query.filter_by(login=log.upper()).first()
            if rec:
                return 2
            else:
                rec=User(log,passw)                 
                self.dbase.session.add(rec)
                self.dbase.session.commit()
                return 1
        except Exception as exc:
            sentry_sdk.capture_exception(exc)
            return 0       
    
    def addobj(self,tok:str,name:str):
            us_id=self.getusr(tok)
            if us_id:
                try:
                    rec=Item(us_id,name,'')                 
                    self.dbase.session.add(rec)
                    self.dbase.session.commit()
                    return [rec.id,name]    
                except Exception as exc:
                    sentry_sdk.capture_exception(exc)
                    return None
            return None     

    def delobj(self,tok:str,id_obj:int):
            us_id=self.getusr(tok)
            if us_id:
                try:
                    rec = Item.query.filter_by(id=id_obj).first()   
                    if rec and rec.usr_id==us_id:
                        self.dbase.session.delete(rec)
                        self.dbase.session.commit()
                        return 1
                except Exception as exc:
                    sentry_sdk.capture_exception(exc)
                    return 0
            return 0

    def listobj(self,tok:str):
            us_id=self.getusr(tok)
            if us_id:
                try:
                    rec = Item.query.filter_by(usr_id=us_id).all() 
                    return [[x.id,x.name] for x in rec]    
                except:
                    return None
            return None   

    def getobj(self,log:str,idobj:int,tok:str):
            us_id=self.getusr(tok)
            try:
                rec= User.query.filter_by(login=log.upper()).first()
                if rec and us_id and idobj:
                    rec1 = Item.query.filter_by(id=idobj).first() 
                    if rec1:
                        self.dtoken=Ttoken('hw_22_key',120)
                        return [rec1.id,self.dtoken.dumps({'usr_id':rec.id}).decode('utf-8')]
            except Exception as exc:
                sentry_sdk.capture_exception(exc)
                return None
            return None 

    def movobj(self,par,tok:str):
            try: 
                if self.dtoken: 
                    us_id=self.dtoken.loads(tok)['usr_id'] 
                if par and us_id:
                    rec = Item.query.filter_by(id=par).first() 
                    if rec:
                        rec.usr_id=us_id
                        self.dbase.session.commit()
                        self.dtoken=None
                        return 1   
            except Exception as exc:
                sentry_sdk.capture_exception(exc)
                return 0
            return 0                
               
                

dwork=Dbwork(db)        



        