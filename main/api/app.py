from flask import Flask
from api import Config,init_db
from dotenv import load_dotenv



app = Flask(__name__)
load_dotenv()
app.config.from_object(Config)
init_db(app)