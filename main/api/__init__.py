import os
from flask_sqlalchemy import SQLAlchemy
from dotenv import load_dotenv
load_dotenv()

class Config:
    SQLALCHEMY_DATABASE_URI= f"postgresql://{os.getenv('POSTGRES_USER')}:{os.getenv('POSTGRES_PASSWORD')}@{os.getenv('POSTGRES_HOST')}:{os.getenv('POSTGRES_PORT')}/{os.getenv('POSTGRES_DB')}"
    SQLALCHEMY_TRACK_MODIFICATIONS =False

class Test:
    SQLALCHEMY_DATABASE_URI = "sqlite:///hw22_lite.db"
    SQLALCHEMY_TRACK_MODIFICATIONS =False


db = SQLAlchemy() 

class User(db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(100),nullable=False,unique=True)
    passwrd = db.Column(db.String(100),nullable=False)
    
    def __init__(self, login:str, passwrd:str) -> None:
        self.login = login.upper()
        self.passwrd = passwrd

class Item(db.Model):
    __tablename__ = "items"
    id = db.Column(db.Integer, primary_key=True)
    usr_id = db.Column(db.Integer,db.ForeignKey('users.id'),nullable=False)
    name = db.Column(db.String(100),nullable=False)
    value = db.Column(db.String(100))
    
    def __init__(self, usr_id:int, name:str,val:str) -> None:
        self.usr_id = usr_id
        self.name = name
        self.value = val

def init_db(app):
    db.init_app(app)
    with app.app_context():
        db.create_all()

