from flask import Blueprint, jsonify, request
from api.dbwork import dwork
import requests

view = Blueprint("view", __name__)
item = Blueprint("item", __name__)

@view.route("/")
def home():
    return 'working dbapi_22'

@view.route("/registration",methods=["POST"])
def reg():
    st=dwork.adduser(request.json['login'],request.json['password'])
    return jsonify({'status':st})

@view.route("/login",methods=["POST"])
def login():
    tag=dwork.gettok(request.json['login'],request.json['password'])
    return jsonify({'tag':tag})         

@view.route("/send",methods=["POST"])
def snd():
    try:
        data=dwork.getobj(request.json['login'],request.json['id'],request.json['tag'])
        if data:
            req=requests.get('http://ifconfig.me/ip').text.strip()
            return jsonify({'ref':'http://'+req+':83/get/'+str(data[0]),'tag':data[1]})              
        else:
            return 'error'    
    except:
        return 'not found'        
        
@view.route("/get/<param>",methods=["GET"])
def geta(param):
  try:  
    if param:
        return jsonify({'status':dwork.movobj(param,request.json['tag'])})
    return jsonify({'status':0})                  
  except:
        return 'error' 

@item.route("/new",methods=["POST"])
def new():
    try:
        data = dwork.addobj(request.json['tag'],request.json['name'])
        if data:
            return jsonify({'status':1,'id':data[0],'name':data[1]})  
        return jsonify({'status':0})    
    except:
        return 'error'     

@item.route("/:id",methods=["DELETE"])
def dela():
    try:
        return jsonify({'status':dwork.delobj(request.json['tag'],request.json['id'])})  
    except:
        return 'error'        

@item.route("/",methods=["GET"])
def getl():
    try:
        return jsonify([{'id':x[0],'name':x[1]} for x in dwork.listobj(request.json['tag'])])       
    except:
        return 'error'       

