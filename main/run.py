from api.app import app
from api.routes import view,item
app.register_blueprint(view)
app.register_blueprint(item,url_prefix='/items')
if __name__ == "__main__":
    app.run(host="0.0.0.0",debug=False,port=5000)
